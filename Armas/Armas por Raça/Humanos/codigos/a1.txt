<weapon>
  <part id="LReceiver018B" x="808.05" y="520.3" rotation="-180" scale="1" flip="1,1" colors="202402,a0a60a"/>
  <group x="1312.9" y="489.4" rotation="0" scale="0.8" flip="1,1">
    <part id="ISight031A" x="0.55" y="-15.75" rotation="0" scale="1" flip="1,1" colors="a0a60a"/>
    <part id="ISight031B" x="0" y="0" rotation="0" scale="1" flip="1,1" colors="a0a60a,202402"/>
  </group>
  <part id="UReceiver020" x="994.3" y="534.7" rotation="0" scale="1" flip="1,1" colors="202402,a0a60a"/>
  <part id="FlashHider011" x="1021.6" y="516.25" rotation="180" scale="1.9075927734375" flip="1,1" colors="202402"/>
  <part id="FlashHider011" x="1224.5" y="517.75" rotation="180" scale="1.9075997265261428" flip="1,1" colors="202402"/>
  <part id="LReceiver021" x="867.75" y="583.8" rotation="0" scale="1" flip="1,1" colors="a0a60a,202402,202402"/>
  <part id="RMag020" x="918.3" y="565.65" rotation="-107.02250593876516" scale="1" flip="1,-1" colors="202402,a0a60a"/>
  <part id="LReceiver007" x="837.3" y="566.8" rotation="0" scale="1" flip="1,1" colors="202402,a0a60a"/>
</weapon>
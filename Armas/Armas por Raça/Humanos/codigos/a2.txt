<weapon>
  <part id="LReceiver018B" x="808.05" y="520.3" rotation="-180" scale="1" flip="1,1" colors="202402,a0a60a"/>
  <group x="1312.9" y="489.4" rotation="0" scale="0.8" flip="1,1">
    <part id="ISight031A" x="0.55" y="-15.75" rotation="0" scale="1" flip="1,1" colors="a0a60a"/>
    <part id="ISight031B" x="0" y="0" rotation="0" scale="1" flip="1,1" colors="a0a60a,202402"/>
  </group>
  <part id="UReceiver020" x="994.3" y="534.7" rotation="0" scale="1" flip="1,1" colors="202402,a0a60a"/>
  <part id="FlashHider011" x="1021.6" y="516.25" rotation="180" scale="1.9075927734375" flip="1,1" colors="202402"/>
  <part id="FlashHider011" x="1233.1" y="519.15" rotation="180" scale="2.2" flip="1,1" colors="202402"/>
  <part id="UReceiver025" x="1136.15" y="565.05" rotation="0" scale="1.5" flip="-1,-1" colors="202402"/>
  <part id="LReceiver021" x="867.75" y="583.8" rotation="0" scale="1" flip="1,1" colors="a0a60a,202402,202402"/>
  <part id="RMag020" x="918.3" y="565.65" rotation="-107.02250593876516" scale="1" flip="1,-1" colors="202402,a0a60a"/>
  <part id="LReceiver007" x="837.3" y="566.8" rotation="0" scale="1" flip="1,1" colors="202402,a0a60a"/>
  <part id="RIS017" x="1215.6" y="544" rotation="0" scale="1" flip="1,1" colors="202402,202402"/>
  <part id="FlashHider011" x="1074.15" y="570.4" rotation="180" scale="2.1999969482421875" flip="1,1" colors="202402"/>
  <part id="FlashHider011" x="1221.55" y="564.55" rotation="180" scale="2.1999969482421875" flip="1,1" colors="202402"/>
  <group x="1002.65" y="470" rotation="180" scale="1" flip="1,1">
    <part id="Foregrip004C" x="0" y="38.65" rotation="0" scale="1" flip="1,1" colors="202402"/>
    <part id="Foregrip004B" x="0" y="16.35" rotation="0" scale="1" flip="1,1" colors="202402"/>
    <part id="Foregrip004A" x="0" y="-22.25" rotation="0" scale="1" flip="1,1" colors="202402"/>
  </group>
</weapon>
<weapon>
  <part id="ISight017B" x="987.3" y="591.75" rotation="0" scale="0.8620787503077127" flip="-1,-1" colors="444444,999999"/>
  <part id="ISight017B" x="1055.55" y="565.3" rotation="90" scale="0.7498497684150918" flip="-1,-1" colors="555555,333333"/>
  <part id="ISight028A" x="996.3" y="558.4" rotation="0" scale="0.5" flip="-1,1" colors="555555,222222"/>
  <part id="ISight028A" x="984.65" y="561.45" rotation="-180" scale="0.5654144287109375" flip="-1,-1" colors="555555,222222"/>
  <part id="ISight028A" x="991.15" y="539.5" rotation="0" scale="0.5654225619036559" flip="-1,1" colors="555555,222222"/>
  <part id="AStock03B" x="1021.75" y="551.45" rotation="0" scale="0.3959503173828125" flip="-1,1" colors="404040"/>
  <part id="AStock03B" x="1038.75" y="551.45" rotation="0" scale="0.3959629664893944" flip="1,1" colors="404040"/>
  <part id="AStock03B" x="987.85" y="556.85" rotation="90" scale="0.687255859375" flip="1,1" colors="555555"/>
  <part id="AStock03B" x="992.85" y="556.85" rotation="90" scale="0.6872646257843804" flip="1,1" colors="555555"/>
  <group x="663.3" y="546.25" rotation="0" scale="0.5064758087256378" flip="1,1">
    <group x="-27.05" y="-4" rotation="0" scale="0.490875244140625" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
    <group x="27.05" y="-4" rotation="0" scale="0.490875244140625" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
    <group x="27.05" y="4" rotation="0" scale="0.490875244140625" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
    <group x="-27.05" y="4" rotation="0" scale="0.490875244140625" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
  </group>
  <group x="649.7" y="540.15" rotation="0" scale="0.24861643625951793" flip="1,1">
    <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
  </group>
  <group x="677.05" y="540.15" rotation="0" scale="0.24861643625951793" flip="1,1">
    <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
  </group>
  <group x="677.05" y="536.1" rotation="0" scale="0.24861643625951793" flip="1,1">
    <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
  </group>
  <group x="649.7" y="536.1" rotation="0" scale="0.24861643625951793" flip="1,1">
    <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
  </group>
  <group x="673.05" y="532" rotation="0" scale="0.24861879483921187" flip="1,1">
    <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
  </group>
  <group x="649.7" y="532" rotation="0" scale="0.24861879483921187" flip="1,1">
    <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
  </group>
  <group x="657.6" y="528" rotation="0" scale="0.45085589912788365" flip="1,1">
    <group x="-17.5" y="0" rotation="0" scale="0.5514373779296875" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
    <group x="17.5" y="0" rotation="0" scale="0.5514373779296875" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
  </group>
  <group x="657.6" y="523.95" rotation="0" scale="0.4508568132158771" flip="1,1">
    <group x="-17.5" y="0" rotation="0" scale="0.5514391797428317" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.24295563879563903" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
    <group x="17.5" y="0" rotation="0" scale="0.5514373779296875" flip="1,1">
      <part id="Barrel026" x="-32" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
      <part id="Barrel026" x="31.95" y="0" rotation="0" scale="0.242950439453125" flip="1,1" colors="444444"/>
    </group>
  </group>
  <part id="AStock03B" x="758.7" y="569.45" rotation="0" scale="0.7229567307021965" flip="1,1" colors="555555"/>
  <group x="926.85" y="555.1" rotation="0" scale="0.048397820467079494" flip="1,1">
    <part id="Bolt011B" x="0" y="0" rotation="0" scale="10.36444091796875" flip="1,1" colors="333333"/>
    <part id="Bolt011B" x="0.55" y="-21.25" rotation="0" scale="7.0961761474609375" flip="1,1" colors="666666"/>
    <part id="Bolt011B" x="0.6" y="-38.5" rotation="0" scale="3.9872894287109375" flip="1,1" colors="333333"/>
    <part id="Bolt011B" x="0.6" y="-52.6" rotation="0" scale="1.3444976806640625" flip="1,-1" colors="666666"/>
  </group>
  <part id="ISight017A" x="799.8" y="519.15" rotation="-90" scale="0.5078554761226509" flip="1,-1" colors="444444"/>
  <part id="ISight017A" x="799.8" y="512.4" rotation="-90" scale="0.5078554761226509" flip="1,-1" colors="444444"/>
  <part id="ISight017A" x="685.25" y="519.15" rotation="-90" scale="0.5078554761226509" flip="1,-1" colors="444444"/>
  <part id="ISight017A" x="685.1" y="512.65" rotation="-90" scale="0.5078554761226509" flip="1,-1" colors="444444"/>
  <part id="AStock05C" x="703.8" y="517.25" rotation="-180" scale="0.3768906058316513" flip="-1,-1" colors="333333"/>
  <part id="AStock05C" x="730.05" y="516.95" rotation="-180" scale="0.3768906058316513" flip="-1,-1" colors="333333"/>
  <part id="AStock05C" x="753.05" y="516.95" rotation="180" scale="0.3768906058316513" flip="1,-1" colors="333333"/>
  <part id="AStock05C" x="781.3" y="517.25" rotation="180" scale="0.3768906058316513" flip="1,-1" colors="333333"/>
  <part id="AStock03B" x="731.8" y="517.75" rotation="180" scale="1.2041162336755222" flip="1,-1" colors="333333"/>
  <part id="AStock03B" x="753.05" y="517.75" rotation="0" scale="1.2041162336755222" flip="1,1" colors="333333"/>
  <part id="ISight035" x="715.05" y="551.3" rotation="-90" scale="1.446557507195382" flip="1,1" colors="444444"/>
  <part id="AStock03B" x="930.7" y="493.15" rotation="-180" scale="0.28293740590292854" flip="-1,-1" colors="444444"/>
  <part id="RCover02S" x="766.7" y="568" rotation="0" scale="0.5423269634601079" flip="1,1" colors="918382"/>
  <part id="RCover02S" x="801.15" y="568" rotation="0" scale="0.5423269634601079" flip="1,1" colors="918382"/>
  <part id="Barrel026" x="773.85" y="568" rotation="0" scale="0.3348232828466196" flip="1,1" colors="777777"/>
  <group x="792.75" y="558.65" rotation="0" scale="0.06946166400751015" flip="1,1">
    <part id="Bolt011B" x="0" y="0" rotation="0" scale="6.963571585147082" flip="1,1" colors="2d2d2d"/>
    <part id="Bolt011B" x="3.75" y="-8.9" rotation="0" scale="5.114838545326804" flip="1,1" colors="444444"/>
    <part id="Bolt011B" x="6.5" y="-13.8" rotation="0" scale="3.2144090568039165" flip="1,1" colors="666666"/>
  </group>
  <part id="ISight011" x="771.3" y="563.2" rotation="-90" scale="0.6971544370147895" flip="1,-1" colors="666666,999999"/>
  <part id="ISight035" x="762.65" y="564.45" rotation="0" scale="0.8912709898819219" flip="-1,-1" colors="666666"/>
  <part id="RCover02S" x="879.15" y="568" rotation="0" scale="0.5423269634601079" flip="1,1" colors="918382"/>
  <part id="ISight035" x="762.5" y="557.85" rotation="0" scale="0.8912732200926997" flip="1,-1" colors="666666"/>
  <part id="Barrel026" x="863.5" y="568" rotation="0" scale="0.33481686305587055" flip="1,1" colors="777777"/>
  <group x="853.15" y="558.65" rotation="0" scale="0.06946112928506519" flip="1,1">
    <part id="Bolt011B" x="0" y="0" rotation="0" scale="6.96356201171875" flip="1,1" colors="2d2d2d"/>
    <part id="Bolt011B" x="3.75" y="-8.9" rotation="0" scale="5.114837646484375" flip="1,1" colors="444444"/>
    <part id="Bolt011B" x="6.5" y="-13.8" rotation="0" scale="3.2144012451171875" flip="1,1" colors="666666"/>
  </group>
  <part id="RCover02S" x="939.9" y="568" rotation="0" scale="0.5423319442914111" flip="1,1" colors="918382"/>
  <part id="Barrel026" x="936.15" y="568" rotation="0" scale="0.33481686305587055" flip="1,1" colors="777777"/>
  <part id="Bipod02C" x="952.7" y="563.75" rotation="0" scale="1.9060078232727669" flip="1,1" colors="444444"/>
  <part id="AStock03B" x="854.85" y="601.3" rotation="0" scale="0.35204303764158595" flip="1,1" colors="333333"/>
  <part id="AStock03B" x="846.05" y="601.25" rotation="0" scale="0.35205047762874025" flip="1,1" colors="333333"/>
  <part id="AStock04C" x="699.6" y="580.75" rotation="145.04237280849412" scale="0.7784623832064365" flip="1,1" colors="444444"/>
  <part id="GasBlock01" x="963.7" y="517.85" rotation="90" scale="0.4245185215289048" flip="1,1" colors="666666"/>
  <part id="GasBlock01" x="964.35" y="517.75" rotation="90" scale="0.5127009753416429" flip="1,1" colors="333333"/>
  <part id="ISight035" x="661" y="603.9" rotation="0" scale="1.2353983567911098" flip="-1,-1" colors="444444"/>
  <part id="AStock03B" x="907.95" y="493.25" rotation="0" scale="0.2829398506150246" flip="-1,1" colors="444444"/>
  <part id="Bipod02C" x="870" y="563.75" rotation="0" scale="1.9060097262927047" flip="1,1" colors="444444"/>
  <part id="CStock13C" x="804.65" y="561.2" rotation="0" scale="1.2468249898945667" flip="-1,-1" colors="444444,222222"/>
  <part id="AStock01C" x="935.3" y="576.35" rotation="0" scale="0.8704823051436631" flip="1,1" colors="444444,666666"/>
  <part id="Bolt006" x="785.8" y="545.25" rotation="0" scale="0.6981291330051009" flip="1,1" colors="444444,e1e1e1"/>
  <part id="Bolt005" x="859.35" y="554" rotation="0" scale="1.3309848090326732" flip="1,1" colors="444444,e1e1e1"/>
  <part id="AStock01C" x="832.85" y="518.95" rotation="-5.19817299810623" scale="0.9469001094807185" flip="1,1" colors="444444,444444"/>
  <part id="AStock01C" x="920.35" y="504" rotation="5.524704981148318" scale="0.8707599132895436" flip="1,1" colors="444444,444444"/>
  <part id="Silencer03" x="963.8" y="519.2" rotation="-90" scale="0.3614234536888878" flip="1,-1" colors="333333,666666"/>
  <part id="ISight004B" x="965.25" y="514.5" rotation="-90" scale="1.1014036202669664" flip="1,-1" colors="222222"/>
  <part id="ISight011" x="967.5" y="506.65" rotation="90" scale="0.773286946994027" flip="-1,-1" colors="555555,9e7f46"/>
  <part id="ISight004B" x="956.05" y="505.65" rotation="-90" scale="0.8004413286638268" flip="1,-1" colors="222222"/>
  <part id="ISight004B" x="972.8" y="527.6" rotation="-90" scale="1.6220048535513585" flip="1,-1" colors="666666"/>
  <part id="ISight035" x="992.7" y="523.55" rotation="0" scale="1.176606447551687" flip="1,-1" colors="555555"/>
  <part id="ISight011" x="846" y="585.65" rotation="90" scale="0.7432670066376623" flip="1,1" colors="666666,999999"/>
  <part id="RMount009" x="836.25" y="569.5" rotation="90" scale="0.7933864365690763" flip="1,1" colors="666666"/>
  <part id="ISight035" x="984.4" y="499.45" rotation="90" scale="0.3659162637397752" flip="-1,1" colors="444444"/>
  <part id="AStock03B" x="915.45" y="585.05" rotation="0" scale="2.0483780853041003" flip="1,-1" colors="444444"/>
  <part id="AStock03B" x="864.6" y="654.25" rotation="-69.18945628246568" scale="0.6918010658801479" flip="1,1" colors="222222"/>
  <part id="AStock03B" x="873.8" y="657.85" rotation="-69.18945628246568" scale="0.6918010658801479" flip="1,1" colors="222222"/>
  <part id="AStock03B" x="884.3" y="615.45" rotation="110.8451027341236" scale="1.889004838865019" flip="1,1" colors="333333"/>
  <part id="Handguard010" x="863.65" y="640.65" rotation="116.85785106879504" scale="0.6909632572188289" flip="1,1" colors="393939,444444"/>
  <part id="Handguard010" x="887.1" y="635.45" rotation="-63.92621983687924" scale="0.6895260615288218" flip="1,1" colors="393939,444444"/>
  <part id="Bolt007B" x="876.85" y="671.05" rotation="139.66932983452972" scale="1.1207009343746956" flip="1,1" colors="333333"/>
  <part id="ChamberLid01" x="859.1" y="683.35" rotation="11.795491126065336" scale="0.7150935738674954" flip="1,-1" colors="333333"/>
  <group x="858.3" y="605.7" rotation="0" scale="0.5064758087256378" flip="1,1">
    <part id="Bolt007B" x="-39.85" y="-8.7" rotation="0" scale="1.0147639045396224" flip="1,1" colors="333333"/>
    <part id="Handguard010" x="4.65" y="3.1" rotation="11.045132059864272" scale="0.7136045739248208" flip="1,1" colors="333333,444444"/>
  </group>
  <part id="Bolt007B" x="865.3" y="618.35" rotation="-78.6479884254547" scale="0.9985202959858236" flip="1,1" colors="222222"/>
  <group x="966.7" y="602.3" rotation="0" scale="0.46465936118362783" flip="1,1">
    <part id="ISight004B" x="0.05" y="0" rotation="98.08348736739867" scale="3.2025299072265625" flip="1,1" colors="444444"/>
    <part id="ISight004B" x="-8.4" y="1" rotation="90" scale="3.2025299072265625" flip="1,1" colors="444444"/>
  </group>
  <part id="Barrel026" x="893.3" y="625.05" rotation="18.45276008168057" scale="0.1396256903962279" flip="1,1" colors="444444"/>
  <part id="FlashHider011" x="922.7" y="632.25" rotation="6.748222396086447" scale="0.21843356506827816" flip="1,1" colors="444444"/>
  <part id="Bolt007B" x="897.5" y="613.95" rotation="76.08938829662554" scale="1.1839594453366586" flip="-1,1" colors="333333"/>
  <part id="Barrel026" x="952.75" y="631.6" rotation="-6.498847669094431" scale="0.13962599229167352" flip="1,1" colors="444444"/>
  <part id="FStock08" x="901.55" y="615.3" rotation="-5.2214234442378995" scale="0.2529244965643132" flip="-1,1" colors="666666,777777,777777"/>
  <part id="GasBlock02" x="923.4" y="603.8" rotation="-2.00677060759525" scale="0.5064758087256378" flip="1,-1" colors="666666"/>
  <part id="ISight004B" x="901.8" y="620.5" rotation="17.630898333068117" scale="1.2009721458006928" flip="1,1" colors="666666"/>
  <part id="GasBlock02" x="919.45" y="612.25" rotation="57.67475884492774" scale="0.46931480144788507" flip="1,1" colors="666666"/>
  <part id="AStock03B" x="988.3" y="563.95" rotation="0" scale="1.8208077267213547" flip="-1,1" colors="555555"/>
  <part id="GasBlock02" x="962.45" y="602.55" rotation="90" scale="0.5064139170793658" flip="1,-1" colors="444444"/>
  <part id="Bolt007B" x="863.8" y="629.35" rotation="-35.811438780223995" scale="0.9985202959858236" flip="1,1" colors="222222"/>
  <part id="Barrel026" x="832.15" y="510.5" rotation="-10.156464885453394" scale="0.5064758087256378" flip="1,1" colors="222222"/>
  <part id="FStock08" x="716.8" y="557.65" rotation="0" scale="1.013575318672108" flip="1,1" colors="404040,333333,333333"/>
  <part id="AStock04A" x="692.75" y="569.4" rotation="0" scale="1.0145839409385964" flip="1,1" colors="444444,444444,444444"/>
  <part id="AStock05A" x="645.2" y="562.2" rotation="-6.407268816225248" scale="1.0691466086189525" flip="1,1" colors="333333,666666"/>
  <part id="ISight027" x="1006.2" y="579.6" rotation="0" scale="1.8671411085240248" flip="1,1" colors="333333"/>
  <part id="AStock03B" x="1006.35" y="567.05" rotation="90" scale="0.8048177686089071" flip="1,1" colors="333333"/>
  <part id="ISight035" x="884.75" y="519.15" rotation="0" scale="1.1420715836644302" flip="1,1" colors="444444"/>
  <part id="ISight035" x="928.7" y="531.55" rotation="0" scale="1.7844001674264824" flip="1,-1" colors="555555"/>
</weapon>